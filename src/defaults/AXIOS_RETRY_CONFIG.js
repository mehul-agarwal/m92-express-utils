'use strict'

import axiosRetry from 'axios-retry'

const AXIOS_RETRY_CONFIG = {
  retryDelay: axiosRetry.exponentialDelay,
  retries: 0
}

export default AXIOS_RETRY_CONFIG
