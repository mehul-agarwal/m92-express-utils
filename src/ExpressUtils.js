'use strict'

import EXPS_CONFIG from './defaults/EXPS_CONFIG'
import EXPS_CONST from './defaults/EXPS_CONST'

import Middleware from './lib/Middleware'
import ReqHandler from './lib/ReqHandler'
import ResHandler from './lib/ResHandler'
import Authenticator from './lib/Authenticator'
import Crypto from './lib/Crypto'
import Logger from './lib/Logger'
import HttpClientCreator from './lib/HttpClientCreator'
import CustomErrorCreator from './lib/CustomErrorCreator'
import HttpContext from './lib/HttpContext'

import ResponseBody from './helpers/ResponseBody'

import ERROR_CLASSIFICATIONS from './constants/ERROR_CLASSIFICATIONS'

import { nanoid } from 'nanoid'
import * as uuid from 'uuid'
import openpgp from 'openpgp'
import axios from 'axios'
import * as crypto from '@m92/crypto'
import expressHttpContext from 'express-http-context'
import * as expressValidator from 'express-validator'
import moment from 'moment-timezone'

export default class ExpressUtils {
  constructor (CONFIG = {}, CONSTANTS = {}, options = {}) {
    const _CONFIG = { ...EXPS_CONFIG, ...CONFIG }
    const _CONSTANTS = { ...EXPS_CONST, ...CONSTANTS }

    this.CONFIG = _CONFIG
    this.CONSTANTS = _CONSTANTS
    this.ERROR_CLASSIFICATIONS = ERROR_CLASSIFICATIONS

    const { TIMEZONE } = _CONFIG
    moment.tz.setDefault(TIMEZONE)

    this.CustomError = new CustomErrorCreator(_CONFIG, _CONSTANTS)
    this.httpContext = new HttpContext(_CONFIG, _CONSTANTS)
    this.logger = new Logger(_CONFIG, _CONSTANTS, this)

    this.reqHandler = new ReqHandler(_CONFIG, _CONSTANTS, this)
    this.resHandler = new ResHandler(_CONFIG, _CONSTANTS, this)

    this.HttpClient = new HttpClientCreator(_CONFIG, _CONSTANTS, this)
    this.crypto = new Crypto(_CONFIG, _CONSTANTS, options.crypto, this)
    this.authenticator = new Authenticator(_CONFIG, _CONSTANTS, this)
    this.middleware = new Middleware(_CONFIG, _CONSTANTS, this)
  }
}

export {
  ExpressUtils,
  ResponseBody,

  nanoid,
  uuid,
  openpgp,
  axios,
  crypto,
  expressHttpContext,
  expressValidator,
  moment
}
