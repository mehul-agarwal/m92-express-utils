'use strict'

import autoBind from 'auto-bind'
import LOGGER from '../constants/LOGGER'
import LOG_TYPES from '../constants/LOG_TYPES'

export default function ReqResLogCreator (BaseLog, thisExpressUtils) {
  const { CONFIG } = BaseLog
  const { LOGGER_REQ_RES_LOG_BODY_ENABLED } = CONFIG
  const { httpContext } = thisExpressUtils

  return class ReqResLog extends BaseLog {
    constructor (request, response) {
      const level = LOGGER.LEVELS.TRACE
      super(LOG_TYPES.REQ_RES_LOG, level)

      autoBind(this)

      const { sessionId, requestId } = this
      const reqProps = this.getRequestProps(request)
      const resProps = this.getResponseProps(request, response)

      const { method, url } = reqProps
      const { statusCode, status, responseMessage, responseTimeInMS } = resProps
      const message = `[${sessionId}, ${requestId}] ${method} ${url} | ${statusCode} ${status} | ${responseMessage} | ${responseTimeInMS}ms`

      Object.assign(this, { sessionId, requestId, message }, reqProps, resProps)
    }

    getRequestProps (request, CONFIG = {}) {
      const {
        url,
        originalUrl,
        method,
        timestamp,
        httpVersionMajor,
        httpVersionMinor,
        ipAddress,
        _remoteAddress,
        connection,
        headers,
        body
      } = request

      const requestUrl = originalUrl || url
      const httpVersion = `${httpVersionMajor}.${httpVersionMinor}`
      const requestIp = ipAddress || _remoteAddress || (connection && connection.remoteAddress) || undefined

      const userAgent = headers['user-agent']

      const reqResBodyLog = httpContext.getReqResBodyLog()
      const reqBody = !LOGGER_REQ_RES_LOG_BODY_ENABLED
        ? {}
        : (reqResBodyLog ? body : {})

      return {
        timestamp,
        url: requestUrl,
        method,
        httpVersion,
        ipAddress: requestIp,
        userAgent,
        reqBody
      }
    }

    getResponseProps (request, response) {
      const timestamp = Date.now()
      const { body = {} } = response
      const statusCode = body.statusCode || response.statusCode
      const status = body.status || response.statusMessage
      const responseMessage = body.message
      const responseTimeInMS = (request.timestamp && (timestamp - request.timestamp)) || -1

      const reqResBodyLog = httpContext.getReqResBodyLog()
      const resBody = !LOGGER_REQ_RES_LOG_BODY_ENABLED
        ? {}
        : (reqResBodyLog ? body : {})

      return {
        responseTimeInMS,
        statusCode,
        status,
        responseMessage,
        resBody
      }
    }
  }
}
