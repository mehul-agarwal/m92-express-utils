'use strict'

import autoBind from 'auto-bind'
import ResponseBody from '../helpers/ResponseBody'

export default class ReqHandler {
  constructor (CONFIG, CONSTANTS, thisExpressUtils) {
    const { CustomError, httpContext } = thisExpressUtils

    this.CONFIG = CONFIG
    this.CONSTANTS = CONSTANTS
    this.CustomError = CustomError
    this.httpContext = httpContext

    autoBind(this)
  }

  extractHeaders (request, response, next) {
    if (response.body) { return process.nextTick(next) }

    this._extractAuth(request)
    this._extractCustomHeaders(request)
    this._setHttpContext(request)

    return process.nextTick(next)
  }

  routeSanity (request, response, next) {
    request.isMatched = true
    process.nextTick(next)
  }

  asyncWrapper (middleware) {
    const { CustomError } = this

    return (request, response, next) => {
      if (response.body) { return process.nextTick(next) }

      return Promise.resolve(middleware(request, response, next)).catch((error) => {
        let responseBody

        if (error.constructor.name === 'ResponseBody') {
          responseBody = error
        } else if (error._isCustomError && error.getResponseBody) {
          responseBody = error.getResponseBody()
        } else if (error._isCustomError) {
          const { statusCode, message, data, error: err } = error
          responseBody = new ResponseBody(statusCode, message, data, err)
        } else {
          const err = new CustomError(error)
          responseBody = err.getResponseBody()
        }

        response.body = responseBody
        next()
      })
    }
  }

  // --------- Extract Request Headers -------------------------------------------
  _extractFromHeader (headers = {}, key = '') {
    let value = headers[key] || ''
    value = value || headers[key.toLowerCase()] || ''
    return value
  }

  _extractAuth (request) {
    const { DEFAULT_AUTH, AUTH_HEADER } = this.CONSTANTS
    const { headers = {}, query = {} } = request

    if (DEFAULT_AUTH !== true) { return }

    const { HEADER_KEY = '', HEADER_PROP = '' } = AUTH_HEADER || {}
    let token = this._extractFromHeader(headers, HEADER_KEY)
    token = token.split('Bearer ')[1] || query.token || ''
    request[HEADER_PROP] = token
  }

  _extractCustomHeaders (request) {
    const { EXTRACT_CUSTOM_HEADERS } = this.CONSTANTS
    const { headers = {}, query = {} } = request

    if (!(EXTRACT_CUSTOM_HEADERS instanceof Array)) { return }

    EXTRACT_CUSTOM_HEADERS.forEach(CUSTOM_HEADER => {
      const { HEADER_KEY, REQUEST_PROP, QUERY_KEY } = CUSTOM_HEADER

      let value = this._extractFromHeader(headers, HEADER_KEY)
      if (QUERY_KEY) {
        value = value || query[QUERY_KEY]
      }

      request[REQUEST_PROP] = value
    })
  }

  _setHttpContext (request) {
    const { httpContext } = this
    const { DEFAULT_AUTH, AUTH_HEADER, ACCESS_TOKEN_PROP, UID_REQUEST_KEY } = this.CONSTANTS

    const requestKey = (DEFAULT_AUTH && AUTH_HEADER.REQUEST_PROP) || ACCESS_TOKEN_PROP
    const requestToken = request[requestKey]
    httpContext.setRequestToken(requestToken)

    const uid = request[UID_REQUEST_KEY]
    httpContext.setUid(uid)
  }
  // -----------------------------------------------------------------------------
}
