'use strict'

import autoBind from 'auto-bind'
import ResponseBody from '../helpers/ResponseBody'

export default class ResHandler {
  constructor (CONFIG, CONSTANTS, expressUtils) {
    const { logger } = expressUtils

    this.CONFIG = CONFIG
    this.CONSTANTS = CONSTANTS
    this.logger = logger

    autoBind(this)
  }

  setHeaders (request, response, next) {
    this._exposeHeaders(response)
    this._setCustomHeaders(response)

    return process.nextTick(next)
  }

  handleResponse (request, response, next) {
    const resBody = response.encryptedBody || response.body || {}
    const { statusCode } = resBody

    const handler = ([301, 302].indexOf(statusCode) > -1)
      ? this._redirectResponse
      : this._sendResponse

    handler(request, response, next)
  }

  // --------- Set Response Headers ----------------------------------------------
  _exposeHeaders (response) {
    const { EXPOSE_HEADERS } = this.CONSTANTS
    const { HEADER_KEY = '', HEADER_VALUE = '' } = EXPOSE_HEADERS || {}
    response.set(HEADER_KEY, HEADER_VALUE)
  }

  _setCustomHeaders (response) {
    const { SET_CUSTOM_HEADERS } = this.CONSTANTS

    if (!(SET_CUSTOM_HEADERS instanceof Array)) { return }

    SET_CUSTOM_HEADERS.forEach(CUSTOM_HEADER => {
      const { HEADER_KEY, RESPONSE_PROP } = CUSTOM_HEADER

      const value = response[RESPONSE_PROP] || ''
      response.set(HEADER_KEY, value)
    })
  }
  // -----------------------------------------------------------------------------

  // --------- Handle Response -------------------------------------------------
  _sendResponse (request, response, next) {
    let resBody = response.encryptedBody || response.body || {}
    const { statusCode } = resBody

    if (!resBody || !statusCode) {
      resBody = new ResponseBody(500, 'Response Data Not Found!')
    }

    response.status(resBody.statusCode).json(resBody)
  }

  _redirectResponse (request, response, next) {
    const resBody = response.encryptedBody || response.body || {}
    const { statusCode, data } = resBody
    response.status(statusCode).redirect(data)
  }
  // -----------------------------------------------------------------------------
}
