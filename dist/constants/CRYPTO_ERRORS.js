'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ERROR_CLASSIFICATIONS = _interopRequireDefault(require("./ERROR_CLASSIFICATIONS"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CRYPTO_ERRORS = {
  INVALID_PASSPHRASE_FUNC: {
    statusCode: 500,
    message: "Provided 'getPassphrase' Option is not a function",
    classification: _ERROR_CLASSIFICATIONS.default.CRYTOGRAPHIC
  },
  INVALID_PUBLIC_KEY_FUNC: {
    statusCode: 500,
    message: "Provided 'getPublicKeyArmored' Option is not a function",
    classification: _ERROR_CLASSIFICATIONS.default.CRYTOGRAPHIC
  },
  INVALID_PRIVATE_KEY_FUNC: {
    statusCode: 500,
    message: "Provided 'getPrivateKeyArmored' Option is not a function",
    classification: _ERROR_CLASSIFICATIONS.default.CRYTOGRAPHIC
  },
  PAYLOAD_ENCRYPTION: {
    statusCode: 500,
    message: 'Error Encrypting Payload',
    classification: _ERROR_CLASSIFICATIONS.default.CRYTOGRAPHIC
  },
  PAYLOAD_DECRYPTION: {
    statusCode: 500,
    message: 'Error Decrypting Payload',
    classification: _ERROR_CLASSIFICATIONS.default.CRYTOGRAPHIC
  },
  CREDENTIAL_DECRYPTION: {
    statusCode: 500,
    message: 'Error Decrypting Credentials',
    classification: _ERROR_CLASSIFICATIONS.default.CRYTOGRAPHIC
  }
};
var _default = CRYPTO_ERRORS;
exports.default = _default;