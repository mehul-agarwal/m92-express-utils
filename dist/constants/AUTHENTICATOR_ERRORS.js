'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ERROR_CLASSIFICATIONS = _interopRequireDefault(require("./ERROR_CLASSIFICATIONS"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AUTHENTICATOR_ERRORS = {
  TOKEN_NOT_FOUND: {
    statusCode: 401,
    message: 'Authentication Token Not Found',
    classification: _ERROR_CLASSIFICATIONS.default.AUTHENTICATION
  },
  API_KEY_NOT_FOUND: {
    statusCode: 401,
    message: 'API Key Not Found',
    classification: _ERROR_CLASSIFICATIONS.default.AUTHENTICATION
  }
};
var _default = AUTHENTICATOR_ERRORS;
exports.default = _default;