'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExpressUtils = void 0;
Object.defineProperty(exports, "ResponseBody", {
  enumerable: true,
  get: function get() {
    return _ResponseBody.default;
  }
});
Object.defineProperty(exports, "axios", {
  enumerable: true,
  get: function get() {
    return _axios.default;
  }
});
exports.default = exports.crypto = void 0;
Object.defineProperty(exports, "expressHttpContext", {
  enumerable: true,
  get: function get() {
    return _expressHttpContext.default;
  }
});
exports.expressValidator = void 0;
Object.defineProperty(exports, "moment", {
  enumerable: true,
  get: function get() {
    return _momentTimezone.default;
  }
});
Object.defineProperty(exports, "nanoid", {
  enumerable: true,
  get: function get() {
    return _nanoid.nanoid;
  }
});
Object.defineProperty(exports, "openpgp", {
  enumerable: true,
  get: function get() {
    return _openpgp.default;
  }
});
exports.uuid = void 0;

var _EXPS_CONFIG = _interopRequireDefault(require("./defaults/EXPS_CONFIG"));

var _EXPS_CONST = _interopRequireDefault(require("./defaults/EXPS_CONST"));

var _Middleware = _interopRequireDefault(require("./lib/Middleware"));

var _ReqHandler = _interopRequireDefault(require("./lib/ReqHandler"));

var _ResHandler = _interopRequireDefault(require("./lib/ResHandler"));

var _Authenticator = _interopRequireDefault(require("./lib/Authenticator"));

var _Crypto = _interopRequireDefault(require("./lib/Crypto"));

var _Logger = _interopRequireDefault(require("./lib/Logger"));

var _HttpClientCreator = _interopRequireDefault(require("./lib/HttpClientCreator"));

var _CustomErrorCreator = _interopRequireDefault(require("./lib/CustomErrorCreator"));

var _HttpContext = _interopRequireDefault(require("./lib/HttpContext"));

var _ResponseBody = _interopRequireDefault(require("./helpers/ResponseBody"));

var _ERROR_CLASSIFICATIONS = _interopRequireDefault(require("./constants/ERROR_CLASSIFICATIONS"));

var _nanoid = require("nanoid");

var uuid = _interopRequireWildcard(require("uuid"));

exports.uuid = uuid;

var _openpgp = _interopRequireDefault(require("openpgp"));

var _axios = _interopRequireDefault(require("axios"));

var crypto = _interopRequireWildcard(require("@m92/crypto"));

exports.crypto = crypto;

var _expressHttpContext = _interopRequireDefault(require("express-http-context"));

var expressValidator = _interopRequireWildcard(require("express-validator"));

exports.expressValidator = expressValidator;

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ExpressUtils {
  constructor() {
    var CONFIG = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var CONSTANTS = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    var _CONFIG = _objectSpread(_objectSpread({}, _EXPS_CONFIG.default), CONFIG);

    var _CONSTANTS = _objectSpread(_objectSpread({}, _EXPS_CONST.default), CONSTANTS);

    this.CONFIG = _CONFIG;
    this.CONSTANTS = _CONSTANTS;
    this.ERROR_CLASSIFICATIONS = _ERROR_CLASSIFICATIONS.default;
    var {
      TIMEZONE
    } = _CONFIG;

    _momentTimezone.default.tz.setDefault(TIMEZONE);

    this.CustomError = new _CustomErrorCreator.default(_CONFIG, _CONSTANTS);
    this.httpContext = new _HttpContext.default(_CONFIG, _CONSTANTS);
    this.logger = new _Logger.default(_CONFIG, _CONSTANTS, this);
    this.reqHandler = new _ReqHandler.default(_CONFIG, _CONSTANTS, this);
    this.resHandler = new _ResHandler.default(_CONFIG, _CONSTANTS, this);
    this.HttpClient = new _HttpClientCreator.default(_CONFIG, _CONSTANTS, this);
    this.crypto = new _Crypto.default(_CONFIG, _CONSTANTS, options.crypto, this);
    this.authenticator = new _Authenticator.default(_CONFIG, _CONSTANTS, this);
    this.middleware = new _Middleware.default(_CONFIG, _CONSTANTS, this);
  }

}

exports.ExpressUtils = exports.default = ExpressUtils;