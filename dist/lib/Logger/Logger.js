'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(require("util"));

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _BaseLogCreator = _interopRequireDefault(require("./classes/BaseLogCreator"));

var _ReqResLogCreator = _interopRequireDefault(require("./classes/ReqResLogCreator"));

var _CodeFlowLogCreator = _interopRequireDefault(require("./classes/CodeFlowLogCreator"));

var _LOGGER = _interopRequireDefault(require("./constants/LOGGER"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var {
  LEVELS,
  MODES
} = _LOGGER.default;

class Logger {
  constructor(CONFIG, CONSTANTS, thisExpressUtils) {
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    var BaseLog = new _BaseLogCreator.default(CONFIG, CONSTANTS, thisExpressUtils);
    this.ReqResLog = new _ReqResLogCreator.default(BaseLog, thisExpressUtils);
    this.CodeFlowLog = new _CodeFlowLogCreator.default(BaseLog);
    (0, _autoBind.default)(this);

    this._setConsoleReference();
  } // Custom logs


  log(level, message, data) {
    var {
      CONFIG,
      CodeFlowLog
    } = this;
    var {
      LOGGER_CODE_FLOW_LOG_ENABLED
    } = CONFIG;

    if (!LOGGER_CODE_FLOW_LOG_ENABLED) {
      return;
    }

    message = _util.default.format(message);
    var codeFlowLog = new CodeFlowLog(level, message, data);

    this._dispatch(codeFlowLog);
  }

  fatal(message, data) {
    this.log(LEVELS.FATAL, message, data);
  }

  error(message, data) {
    this.log(LEVELS.ERROR, message, data);
  }

  warn(message, data) {
    this.log(LEVELS.WARN, message, data);
  }

  info(message, data) {
    this.log(LEVELS.INFO, message, data);
  }

  debug(message, data) {
    this.log(LEVELS.DEBUG, message, data);
  }

  trace(message, data) {
    this.log(LEVELS.TRACE, message, data);
  }

  _dispatch(logObject) {
    var {
      CONFIG
    } = this;

    switch (CONFIG.LOGGER_MODE) {
      case MODES.CONSOLE:
        logObject.print();
        break;

      case MODES.CONSOLE_VERBOSE:
        logObject.print(true);
        break;
    }
  } // Reference to default 'console' methods


  _setConsoleReference() {
    this.consoleLog = console.log;
    this.consoleError = console.error;
    this.consoleWarn = console.warn;
    this.consoleInfo = console.info;
    this.consoleDebug = console.debug;
  } // Handle Console logs


  _overrideConsole() {
    var _this = this;

    console.log = function () {
      return _this.log(LEVELS.INFO, _util.default.format(...arguments));
    };

    console.error = function () {
      return _this.log(LEVELS.ERROR, _util.default.format(...arguments));
    };

    console.warn = function () {
      return _this.log(LEVELS.WARN, _util.default.format(...arguments));
    };

    console.info = function () {
      return _this.log(LEVELS.INFO, _util.default.format(...arguments));
    };

    console.debug = function () {
      return _this.log(LEVELS.DEBUG, _util.default.format(...arguments));
    };
  }

}

exports.default = Logger;