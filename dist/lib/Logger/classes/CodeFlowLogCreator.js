'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = CodeFlowLogCreator;

var _LOG_TYPES = _interopRequireDefault(require("../constants/LOG_TYPES"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function CodeFlowLogCreator(BaseLog) {
  return class CodeFlowLog extends BaseLog {
    constructor() {
      var level = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var data = arguments.length > 2 ? arguments[2] : undefined;
      super(_LOG_TYPES.default.CODE_FLOW_LOG, level);
      var msg = message || '';
      this.message = typeof msg === 'string' || msg instanceof String ? msg.toString() : JSON.stringify(msg);
      this.data = data || {};
    }

  };
}