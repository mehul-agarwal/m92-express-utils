'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _crypto = require("@m92/crypto");

var _ResponseBody = _interopRequireDefault(require("../helpers/ResponseBody"));

var _CRYPTO_ERRORS = _interopRequireDefault(require("../constants/CRYPTO_ERRORS"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

class Crypto {
  constructor(CONFIG, CONSTANTS) {
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var thisExpressUtils = arguments.length > 3 ? arguments[3] : undefined;
    var {
      CustomError,
      logger
    } = thisExpressUtils;
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    this.options = options;
    this.CustomError = CustomError;
    this.logger = logger;
    (0, _autoBind.default)(this);
  }

  validatePgpOptions() {
    var {
      options,
      CustomError
    } = this;
    var {
      getPassphrase,
      getPublicKeyArmored,
      getPrivateKeyArmored
    } = options;

    if (!getPassphrase || typeof getPassphrase !== 'function') {
      throw new CustomError(undefined, _CRYPTO_ERRORS.default.INVALID_PASSPHRASE_FUNC);
    }

    if (!getPublicKeyArmored || typeof getPublicKeyArmored !== 'function') {
      throw new CustomError(undefined, _CRYPTO_ERRORS.default.INVALID_PUBLIC_KEY_FUNC);
    }

    if (!getPrivateKeyArmored || typeof getPrivateKeyArmored !== 'function') {
      throw new CustomError(undefined, _CRYPTO_ERRORS.default.INVALID_PRIVATE_KEY_FUNC);
    }
  }

  encryptPayload(request, response, next) {
    var {
      CONFIG,
      CONSTANTS,
      CustomError
    } = this;
    var {
      DISABLE_PAYLOAD_CRYPTOGRAPHY
    } = CONFIG;

    if (DISABLE_PAYLOAD_CRYPTOGRAPHY) {
      return process.nextTick(next);
    }

    try {
      var {
        DEFAULT_AUTH,
        AUTH_HEADER,
        ACCESS_TOKEN_PROP,
        ACCESS_TOKEN_HEADER_KEY
      } = CONSTANTS;

      if (!DEFAULT_AUTH && !ACCESS_TOKEN_PROP) {
        return process.nextTick(next);
      }

      if (DEFAULT_AUTH && !AUTH_HEADER.REQUEST_PROP) {
        return process.nextTick(next);
      }

      var tokenProp = DEFAULT_AUTH && AUTH_HEADER.REQUEST_PROP || ACCESS_TOKEN_PROP;
      var tokenHeader = DEFAULT_AUTH && AUTH_HEADER.HEADER_KEY || ACCESS_TOKEN_HEADER_KEY.toLowerCase();
      var token = response[tokenProp] || response.get(tokenHeader) || '';
      var {
        body
      } = response;

      if (!body || !token) {
        return process.nextTick(next);
      }

      var encryptionKey = _crypto.AesUtils.extractKeyFromToken(token);

      var bodyString = JSON.stringify(body);
      var encryptParams = {
        key: encryptionKey,
        data: bodyString
      };

      var encryptedDataObj = _crypto.Aes.encrypt('aes-256-gcm', encryptParams);

      var {
        payload
      } = encryptedDataObj;
      var encryptedBody = {
        payload
      };
      var encryptedResponseBody = new _ResponseBody.default(200, undefined, encryptedBody);
      response.encryptedBody = encryptedResponseBody;
      return process.nextTick(next);
    } catch (e) {
      var error = new CustomError(e, _CRYPTO_ERRORS.default.PAYLOAD_ENCRYPTION);
      response.body = error;
      return process.nextTick(next);
    }
  }

  decryptPayload(request, response, next) {
    if (response.body) {
      return process.nextTick(next);
    }

    var {
      CONFIG,
      CONSTANTS,
      CustomError
    } = this;
    var {
      DISABLE_PAYLOAD_CRYPTOGRAPHY
    } = CONFIG;
    var {
      DEFAULT_AUTH,
      AUTH_HEADER,
      ACCESS_TOKEN_PROP,
      ACCESS_TOKEN_HEADER_KEY
    } = CONSTANTS;

    if (DISABLE_PAYLOAD_CRYPTOGRAPHY) {
      return process.nextTick(next);
    }

    if (!DEFAULT_AUTH && !ACCESS_TOKEN_PROP) {
      return process.nextTick(next);
    }

    if (DEFAULT_AUTH && !AUTH_HEADER.REQUEST_PROP) {
      return process.nextTick(next);
    }

    try {
      var {
        body
      } = request;
      var tokenProp = DEFAULT_AUTH && AUTH_HEADER.REQUEST_PROP || ACCESS_TOKEN_PROP;
      var tokenHeader = DEFAULT_AUTH && AUTH_HEADER.HEADER_KEY || ACCESS_TOKEN_HEADER_KEY.toLowerCase();
      var token = request[tokenProp] || request.get(tokenHeader) || '';

      if (!body || !token) {
        return process.nextTick(next);
      }

      var decryptionKey = _crypto.AesUtils.extractKeyFromToken(token);

      var {
        payload = ''
      } = body;

      if (!payload) {
        return process.nextTick(next);
      }

      var decryptParams = {
        key: decryptionKey,
        payload
      };

      var decryptedDataObj = _crypto.Aes.decrypt('aes-256-gcm', decryptParams);

      var {
        data: decryptedDataString
      } = decryptedDataObj;
      var decryptedData = JSON.parse(decryptedDataString);
      request.body = decryptedData;
      return process.nextTick(next);
    } catch (e) {
      var error = new CustomError(e, _CRYPTO_ERRORS.default.PAYLOAD_DECRYPTION);
      response.body = error;
      return process.nextTick(next);
    }
  }

  decryptCredentials(request, response, next) {
    var _this = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        options,
        CustomError
      } = _this;
      var {
        getPassphrase,
        getPrivateKeyArmored
      } = options;
      var {
        DISABLE_CREDENTIAL_CRYPTOGRAPHY
      } = CONFIG;

      if (DISABLE_CREDENTIAL_CRYPTOGRAPHY) {
        return process.nextTick(next);
      }

      try {
        var {
          body
        } = request;

        if (!body) {
          return process.nextTick(next);
        }

        _this.validatePgpOptions();

        var passphrase = getPassphrase.constructor.name === 'AsyncFunction' ? yield getPassphrase() : getPassphrase();
        var privateKeyArmored = getPrivateKeyArmored.constructor.name === 'AsyncFunction' ? yield getPrivateKeyArmored() : getPrivateKeyArmored();
        var decryptParams = {
          data: body,
          privateKeyArmored,
          passphrase
        };
        var decryptedData = yield _crypto.Pgp.decrypt(decryptParams);
        request.body = decryptedData;
        return next();
      } catch (e) {
        var error = new CustomError(e, _CRYPTO_ERRORS.default.CREDENTIAL_DECRYPTION);
        response.body = error;
        return process.nextTick(next);
      }
    })();
  }

}

exports.default = Crypto;