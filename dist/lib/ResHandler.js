'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _ResponseBody = _interopRequireDefault(require("../helpers/ResponseBody"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ResHandler {
  constructor(CONFIG, CONSTANTS, expressUtils) {
    var {
      logger
    } = expressUtils;
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    this.logger = logger;
    (0, _autoBind.default)(this);
  }

  setHeaders(request, response, next) {
    this._exposeHeaders(response);

    this._setCustomHeaders(response);

    return process.nextTick(next);
  }

  handleResponse(request, response, next) {
    var resBody = response.encryptedBody || response.body || {};
    var {
      statusCode
    } = resBody;
    var handler = [301, 302].indexOf(statusCode) > -1 ? this._redirectResponse : this._sendResponse;
    handler(request, response, next);
  } // --------- Set Response Headers ----------------------------------------------


  _exposeHeaders(response) {
    var {
      EXPOSE_HEADERS
    } = this.CONSTANTS;
    var {
      HEADER_KEY = '',
      HEADER_VALUE = ''
    } = EXPOSE_HEADERS || {};
    response.set(HEADER_KEY, HEADER_VALUE);
  }

  _setCustomHeaders(response) {
    var {
      SET_CUSTOM_HEADERS
    } = this.CONSTANTS;

    if (!(SET_CUSTOM_HEADERS instanceof Array)) {
      return;
    }

    SET_CUSTOM_HEADERS.forEach(CUSTOM_HEADER => {
      var {
        HEADER_KEY,
        RESPONSE_PROP
      } = CUSTOM_HEADER;
      var value = response[RESPONSE_PROP] || '';
      response.set(HEADER_KEY, value);
    });
  } // -----------------------------------------------------------------------------
  // --------- Handle Response -------------------------------------------------


  _sendResponse(request, response, next) {
    var resBody = response.encryptedBody || response.body || {};
    var {
      statusCode
    } = resBody;

    if (!resBody || !statusCode) {
      resBody = new _ResponseBody.default(500, 'Response Data Not Found!');
    }

    response.status(resBody.statusCode).json(resBody);
  }

  _redirectResponse(request, response, next) {
    var resBody = response.encryptedBody || response.body || {};
    var {
      statusCode,
      data
    } = resBody;
    response.status(statusCode).redirect(data);
  } // -----------------------------------------------------------------------------


}

exports.default = ResHandler;