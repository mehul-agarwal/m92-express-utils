'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _ResponseBody = _interopRequireDefault(require("../helpers/ResponseBody"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ReqHandler {
  constructor(CONFIG, CONSTANTS, thisExpressUtils) {
    var {
      CustomError,
      httpContext
    } = thisExpressUtils;
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    this.CustomError = CustomError;
    this.httpContext = httpContext;
    (0, _autoBind.default)(this);
  }

  extractHeaders(request, response, next) {
    if (response.body) {
      return process.nextTick(next);
    }

    this._extractAuth(request);

    this._extractCustomHeaders(request);

    this._setHttpContext(request);

    return process.nextTick(next);
  }

  routeSanity(request, response, next) {
    request.isMatched = true;
    process.nextTick(next);
  }

  asyncWrapper(middleware) {
    var {
      CustomError
    } = this;
    return (request, response, next) => {
      if (response.body) {
        return process.nextTick(next);
      }

      return Promise.resolve(middleware(request, response, next)).catch(error => {
        var responseBody;

        if (error.constructor.name === 'ResponseBody') {
          responseBody = error;
        } else if (error._isCustomError && error.getResponseBody) {
          responseBody = error.getResponseBody();
        } else if (error._isCustomError) {
          var {
            statusCode,
            message,
            data,
            error: err
          } = error;
          responseBody = new _ResponseBody.default(statusCode, message, data, err);
        } else {
          var _err = new CustomError(error);

          responseBody = _err.getResponseBody();
        }

        response.body = responseBody;
        next();
      });
    };
  } // --------- Extract Request Headers -------------------------------------------


  _extractFromHeader() {
    var headers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var value = headers[key] || '';
    value = value || headers[key.toLowerCase()] || '';
    return value;
  }

  _extractAuth(request) {
    var {
      DEFAULT_AUTH,
      AUTH_HEADER
    } = this.CONSTANTS;
    var {
      headers = {},
      query = {}
    } = request;

    if (DEFAULT_AUTH !== true) {
      return;
    }

    var {
      HEADER_KEY = '',
      HEADER_PROP = ''
    } = AUTH_HEADER || {};

    var token = this._extractFromHeader(headers, HEADER_KEY);

    token = token.split('Bearer ')[1] || query.token || '';
    request[HEADER_PROP] = token;
  }

  _extractCustomHeaders(request) {
    var {
      EXTRACT_CUSTOM_HEADERS
    } = this.CONSTANTS;
    var {
      headers = {},
      query = {}
    } = request;

    if (!(EXTRACT_CUSTOM_HEADERS instanceof Array)) {
      return;
    }

    EXTRACT_CUSTOM_HEADERS.forEach(CUSTOM_HEADER => {
      var {
        HEADER_KEY,
        REQUEST_PROP,
        QUERY_KEY
      } = CUSTOM_HEADER;

      var value = this._extractFromHeader(headers, HEADER_KEY);

      if (QUERY_KEY) {
        value = value || query[QUERY_KEY];
      }

      request[REQUEST_PROP] = value;
    });
  }

  _setHttpContext(request) {
    var {
      httpContext
    } = this;
    var {
      DEFAULT_AUTH,
      AUTH_HEADER,
      ACCESS_TOKEN_PROP,
      UID_REQUEST_KEY
    } = this.CONSTANTS;
    var requestKey = DEFAULT_AUTH && AUTH_HEADER.REQUEST_PROP || ACCESS_TOKEN_PROP;
    var requestToken = request[requestKey];
    httpContext.setRequestToken(requestToken);
    var uid = request[UID_REQUEST_KEY];
    httpContext.setUid(uid);
  } // -----------------------------------------------------------------------------


}

exports.default = ReqHandler;