'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = CustomErrorCreator;

var _http = _interopRequireDefault(require("http"));

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _ResponseBody = _interopRequireDefault(require("../helpers/ResponseBody"));

var _ERROR_CLASSIFICATIONS = _interopRequireDefault(require("../constants/ERROR_CLASSIFICATIONS"));

var _excluded = ["toJSON"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var CAN_CAPTURE = typeof Error.captureStackTrace === 'function';
var CAN_STACK = !!new Error().stack;

function CustomErrorCreator(CONFIG, CONSTANTS) {
  var {
    SERVICE_NAME,
    ERROR_NAME
  } = CONFIG;
  return class CustomError extends Error {
    constructor() {
      for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
        params[_key] = arguments[_key];
      }

      var [error = {}, overrideParams = {}, override = true] = params;
      var {
        statusCode,
        message,
        classification,
        code,
        name,
        type,
        data
      } = overrideParams;
      var errorHasKeys = !!Object.keys(error).length;
      var {
        message: errMessage,
        _isCustomError: errIsCustomError,
        msg: errMsg,
        name: errName,
        service: errService,
        statusCode: errStatusCode,
        type: errType,
        classification: errClassification,
        code: errCode,
        error: errError,
        stack: errStack
      } = error || {};
      var hardOverride = override || !errIsCustomError;
      var finalMessage = hardOverride && message || errMessage || errMsg;
      super(finalMessage);
      this._isCustomError = true;
      this.name = hardOverride && name || errName || ERROR_NAME;
      this.service = hardOverride && SERVICE_NAME || errService || SERVICE_NAME;
      this.statusCode = hardOverride && statusCode || errStatusCode || 500;
      this.message = finalMessage;
      this.msg = finalMessage;
      this.type = hardOverride && type || errType || undefined;
      this.classification = hardOverride && classification || errClassification || _ERROR_CLASSIFICATIONS.default.GENERIC;
      this.code = hardOverride && code || errCode || undefined;
      this.status = _http.default.STATUS_CODES[statusCode];
      this.error = errError || (errIsCustomError || !errorHasKeys ? undefined : error);
      var thisErrorHasKeys = !!Object.keys(this.error || {}).length;

      if (!thisErrorHasKeys) {
        this.error = undefined;
      }

      this.data = data;
      this.stack = errStack || CAN_CAPTURE && Error.captureStackTrace(this, CustomError) || CAN_STACK && new Error().stack || undefined;
      (0, _autoBind.default)(this);
    }

    getResponseBody() {
      var {
        statusCode,
        message
      } = this;
      var error = this.toJSON();
      var {
        NODE_ENV
      } = process.env;
      error.stack = NODE_ENV === 'production' && undefined || error.stack;
      return new _ResponseBody.default(statusCode, message, undefined, error);
    }

    toJSON() {
      var _this = this,
          {
        toJSON
      } = _this,
          rest = _objectWithoutProperties(_this, _excluded);

      return JSON.parse(JSON.stringify(rest));
    }

  };
}