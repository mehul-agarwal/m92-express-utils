'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _axiosRetry = _interopRequireDefault(require("axios-retry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AXIOS_RETRY_CONFIG = {
  retryDelay: _axiosRetry.default.exponentialDelay,
  retries: 0
};
var _default = AXIOS_RETRY_CONFIG;
exports.default = _default;